#!/bin/env python
__author__ = 'leonardo'

from tornado.ioloop import IOLoop
from handlers import *

"""
URL_PARSER and SETTINGS are set outside tornado.web.Application call

*** PLEASE MAINTAIN THIS SECTION CLEAN AND DRY!!!! ***

"""

URL_PARSER = [
    (r"/", HomeHandler),
    (r"/new", PublishArticle),
    (r"/login", GoogleAuthHandler),
]

SETTINGS = {
    "cookie_secret": "095c6b50-2995-4004-8868-8e54d9e8fbb3",
    "login_url": "/login",
    "debug": "True"
}

application =   tornado.web.Application(URL_PARSER, **SETTINGS)

if __name__ == '__main__':
    application.listen(8888)
    IOLoop.instance().start()